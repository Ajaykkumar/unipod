# unipod-api project set up
@author Ajaykkumar Rajendran

$ git clone https://Ajaykkumar@bitbucket.org/unipodteam/unipod.git
$ cd unipod/API
$ docker-compose build api
$ docker-compose run --rm api npm install
$ docker-compose run --rm api npm run autoupdate
$ docker-compose up api
