"use strict";

const server = require('../server');
const postgres_ds = server.dataSources.postgres;
const foreignKey = require('../utils/constants/foreignKey');

/**
 * @author Ajaykkumar Rajendran
 */
class ForeignKeyMappingMigration {
  change() {
    // This is a sample method you can do it your own
    postgres_ds.connector.query(foreignKey.foreignKeyConstraint,
      (err) => {
      if (err)
        throw err;
    });
  }
}

let objForeignKeyMapping = new ForeignKeyMappingMigration();
objForeignKeyMapping.change();
