"use strict";

const server = require('../server');
const postgres_ds = server.dataSources.postgres;

const defaultRoles = [
  {
    name: 'USER',
    description: 'Role assigned to a customer-user'
  },
  {
    name: 'SUPER_ADMIN',
    description: 'Role assigned to a super-admin user'
  }
];
/**
 * @author Ajaykkumar Rajendran
 */
class CreateRolesMigration {
  change() {
    // To create set of default roles and persist it into database
    server.models.Roles.create(defaultRoles, (err, roles) => {
      if (err)
        throw err;
    });
  }
}

let objCreateRoles = new CreateRolesMigration();
objCreateRoles.change();
